
# MovieApp

  

## Description

  

MovieApp is a web application designed to help users discover, search for, and explore information about movies. It provides users with a user-friendly interface to browse through a vast database of movies, view details such as plot summaries, cast, ratings, and more. Whether you're a movie enthusiast looking for recommendations or just curious about the latest releases, MovieApp has you covered.

  

## Features

  

-  **Search:** Easily search for movies by title keywords.

-  **Browse:** Explore trending movies, top-rated movies, and upcoming releases.

-  **Details:** Access comprehensive information about each movie title, year, and director.

  

## Technologies Used

  

-  **Frontend:** HTML, CSS, SCSS, TypeScript (React Native)

-  **Backend:** PHP, Laravel

-  **Database:** MySQL

-  **Deployment:** Docker (to be configure)

## Prerequisites

Before you begin installing and running MovieApp, ensure you have the following prerequisites installed:

##### Git
##### XAMPP
##### Composer
##### NodeJS

Ensure that you have these prerequisites met before proceeding with the installation and setup of MovieApp.

## Installation

1. Clone the repository:

```bash
git  clone  https://gitlab.com/sharkmaster999/movieapp.git

```

2. Navigate to application:

```bash
cd  movieapp

```

3. Set up environment variables:

```bash
cp  .env.example  .env

```

4. Start the backend application:

```bash
php artisan migrate
php artisan db:seed
php artisan serve

```
5. Open new terminal

6. Start the frontend application:
```bash
cd path_to_your_app/movieapp/client
npm install
npm start

```

## Usage

- After running, you can visit the application in your web browser at `http://localhost:19006/`.

- Start exploring movies.

If you have any questions, suggestions, or issues, please feel free to contact us at [remultasimpatiko@gmail.com](mailto:remultasimpatiko@gmail.com).