<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/movies', 'App\Http\Controllers\MovieController@create');
Route::get('/movies', 'App\Http\Controllers\MovieController@showAll');
Route::get('/movies/{id}', 'App\Http\Controllers\MovieController@show');
Route::put('/movies/{id}', 'App\Http\Controllers\MovieController@update');
Route::delete('/movies/{id}', 'App\Http\Controllers\MovieController@destroy');
