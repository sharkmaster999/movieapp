<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Movie;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = [
            [
                'title' => 'The Shawshank Redemption',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1994,
                'director' => 'Frank Darabont',
            ],
            [
                'title' => 'The Godfather',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1972,
                'director' => 'Francis Ford Coppola',
            ],
            [
                'title' => 'The Dark Knight',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 2008,
                'director' => 'Christopher Nolan',
            ],
            [
                'title' => 'Pulp Fiction',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1994,
                'director' => 'Quentin Tarantino',
            ],
            [
                'title' => 'Schindler\'s List',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1993,
                'director' => 'Steven Spielberg',
            ],
            [
                'title' => 'Inception',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 2010,
                'director' => 'Christopher Nolan',
            ],
            [
                'title' => 'Fight Club',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1999,
                'director' => 'David Fincher',
            ],
            [
                'title' => 'Forrest Gump',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1994,
                'director' => 'Robert Zemeckis',
            ],
            [
                'title' => 'The Matrix',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1999,
                'director' => 'Lana Wachowski, Lilly Wachowski',
            ],
            [
                'title' => 'The Lord of the Rings: The Fellowship of the Ring',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 2001,
                'director' => 'Peter Jackson',
            ],
            [
                'title' => 'The Silence of the Lambs',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1991,
                'director' => 'Jonathan Demme',
            ],
            [
                'title' => 'The Departed',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 2006,
                'director' => 'Martin Scorsese',
            ],
            [
                'title' => 'Gladiator',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 2000,
                'director' => 'Ridley Scott',
            ],
            [
                'title' => 'Goodfellas',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1990,
                'director' => 'Martin Scorsese',
            ],
            [
                'title' => 'Braveheart',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1995,
                'director' => 'Mel Gibson',
            ],
            [
                'title' => 'The Green Mile',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1999,
                'director' => 'Frank Darabont',
            ],
            [
                'title' => 'Se7en',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1995,
                'director' => 'David Fincher',
            ],
            [
                'title' => 'The Usual Suspects',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1995,
                'director' => 'Bryan Singer',
            ],
            [
                'title' => 'The Sixth Sense',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1999,
                'director' => 'M. Night Shyamalan',
            ],
            [
                'title' => 'American History X',
                'poster' => 'https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg',
                'year' => 1998,
                'director' => 'Tony Kaye',
            ],
        ];

        // Loop through each movie and create a record in the database
        foreach ($movies as $movieData) {
            Movie::create($movieData);
        }
    }
}
