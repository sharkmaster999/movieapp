<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class MovieController extends Controller
{
    // Create a new movie
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'poster' => 'required|string',
            'year' => 'required|integer',
            'director' => 'required|string',
        ]);

        $movie = Movie::create($validatedData);
        return response()->json($movie, 201);
    }

    // Get a specific movie by its ID
    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        return response()->json($movie);
    }

    // Get a all movies
    public function showAll(Request $request)
    {
        // Set default values for pagination and sorting
        $perPage = $request->input('per_page', 10); // Number of items per page, default is 10
        $sortBy = $request->input('sort_by', 'id'); // Default sorting by id
        $sortDirection = $request->input('sort_direction', 'asc'); // Default sort direction ascending
        
        // Query builder for movies
        $query = Movie::query();

        // Searching
        $searchTerm = $request->input('search');
        if ($searchTerm) {
            $query->where('title', 'like', '%' . $searchTerm . '%');
        }

        // Sorting
        $query->orderBy($sortBy, $sortDirection);

        // Pagination
        $movies = $query->paginate($perPage);

        return response()->json($movies);
    }

    // Update a movie
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'poster' => 'required|string',
            'year' => 'required|integer',
            'director' => 'required|string',
        ]);

        $movie = Movie::findOrFail($id);
        $movie->update($validatedData);
        return response()->json($movie, 200);
    }

    // Delete a movie
    public function destroy($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();
        return response()->json(null, 204);
    }
}
