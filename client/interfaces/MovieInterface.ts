export interface MovieInterface {
    id: number;
    title: string;
    poster: string;
    year: number;
    director: string;
}