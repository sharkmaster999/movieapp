import React, { useState, useEffect } from 'react';
import { Pressable, StyleSheet, FlatList, Dimensions, Image, TextInput, Button, TouchableOpacity } from 'react-native';

import { MovieInterface } from '@/interfaces/MovieInterface';
import { Text, View } from '@/components/Themed';
import { Link } from 'expo-router';

const { width } = Dimensions.get('window');
const gap = 60;
const itemPerRow = 2;
const totalGapSize = (itemPerRow - 1) * gap;
const windowWidth = width;
const childWidth = (windowWidth - totalGapSize) / itemPerRow;

export default function TabOneScreen() {
  const [movies, setMovies] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [nextPage, setNextPage] = useState('');

  useEffect(() => {
    fetchMovies();
  }, []);

  const fetchMovies = async () => {
    try {
      const response = await fetch('http://localhost:8000/api/movies');
      const res = await response.json();
      setMovies(res.data);
      setNextPage(res.next_page_url);
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };

  const searchMovie = async () => {
    try {
      const response = await fetch('http://localhost:8000/api/movies?search=' + searchText);
      const res = await response.json();
      setMovies(res.data);
      setNextPage(res.next_page_url);
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };

  const loadMore = async () => {
    try {
      if(nextPage !== null) {
        const response = await fetch(nextPage);
        const res = await response.json();
        if (Array.isArray(movies) && Array.isArray(res.data)) {
          // Concatenate the arrays
          setMovies([...movies, ...res.data] as []);
        } else {
          console.error('Movies or res.data is not an array.');
        }
        setNextPage(res.next_page_url);
      }
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };


  const renderMovieItem = ({ item } : { item: MovieInterface }) => {
    return (
      <Link href={`../movie_description/${item.id}`}>
        <Pressable style={styles.container}>
          <View style={styles.card}>
            <Image source={{ uri: item.poster }} style={styles.image} />
            <Text style={styles.movieTitle}>{item.title}</Text>
          </View>
        </Pressable>
      </Link>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{ backgroundColor: '#fff', padding: 10 }}>
        <TextInput
          style={styles.input}
          onChangeText={setSearchText}
          value={searchText}
          placeholder="Search movie title here"
        />
        <TouchableOpacity onPress={searchMovie} style={styles.button}>
          <Text style={styles.buttonText}>Search</Text>
        </TouchableOpacity>
      </View>
      {
        movies.length === 0 ? (
          <Text style={styles.noMovies}>No movies found. Try search for other title keywords.</Text>
        ) : 
        <FlatList
          data={movies}
          renderItem={renderMovieItem}
          keyExtractor={(item) => item.id.toString()}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.movieList}
        />
      }
      {
        nextPage !== null ?
        <TouchableOpacity onPress={loadMore} style={styles.loadMoreButton}>
          <Text style={styles.loadMoreButtonText}>Load More</Text>
        </TouchableOpacity> : null
      }
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input: {
    height: 40,
    margin: 10,
    borderWidth: 1,
    padding: 10,
  },
  noMovies: {
    margin: 20,
  },
  button: {
    backgroundColor: '#007AFF',
    padding: 10,
    tintColor: '#fff',
    marginHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    alignContent: 'center'
  },
  loadMoreButton: {
    backgroundColor: '#FFF',
    padding: 10,
    marginHorizontal: 10,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  loadMoreButtonText: {
    color: '#222',
    fontWeight: 'bold',
    alignContent: 'center'
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    margin: 10,
    width: childWidth,
    height: 300,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    marginBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  movieTitle: {
    fontSize: 13,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  movieList: {
    alignItems: 'center',
    paddingVertical: 10,
  },
});
