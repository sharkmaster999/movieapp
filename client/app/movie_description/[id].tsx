import { StatusBar } from 'expo-status-bar';
import { Image, Platform, StyleSheet } from 'react-native';

import { Text, View } from '@/components/Themed';
import React, { useEffect, useState } from 'react';
import { MovieInterface } from '@/interfaces/MovieInterface';
import { Stack, useLocalSearchParams } from 'expo-router';

export default function MovieDescriptionScreen() {
  const [data, setData] = useState({} as MovieInterface);
  const [isLoading, setIsLoading] = useState(false);
  const { id } = useLocalSearchParams();

  useEffect(() => {
    setIsLoading(true);
    fetchMovies();
  }, []);

  const fetchMovies = async () => {
    try {
      const response = await fetch('http://localhost:8000/api/movies/' + id);
      const res = await response.json();
      setData(res);
      setIsLoading(false);
    } catch (error) {
      console.error('Error fetching movies:', error);
    }
  };
  return (
    
    <View style={styles.container}>
        <Stack.Screen
            options={{
                title: !isLoading ? data.title : 'Loading...',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}
        />
        {
            isLoading ? <Text>Loading...</Text> :
            <View style={styles.separator}>
                <Image source={{ uri: data.poster }} style={styles.image} />
                <Text style={styles.title}>{data.title} ({data.year})</Text>
                <Text style={styles.director}>{data.director}</Text>
            </View>
        }
        <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  director: {
    fontSize: 15,
  },
  separator: {
    margin: 10,
    height: 1,
  },
  image: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    marginBottom: 10,
  },
});
