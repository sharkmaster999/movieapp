#!/bin/sh

echo "Running movieapp-laravel-container's entrypoint file..."

echo "Modifying user (hack for mac)..."
usermod -u 1000 www-data #a hack for macs

echo "Copying config file if it isn't already present...."
cp -n /var/www/html/docker/laravel/.env.docker /var/www/html/.env

echo "Installing oauth keys..."
php artisan passport:keys

echo "Running db migrations..."
php artisan --verbose migrate --seed

echo "Deleting existing apache pid if present..."
if [ -f "$APACHE_PID_FILE" ]; then
    rm "$APACHE_PID_FILE"
fi

echo "Watching for php file changes..."
node docker/laravel/test_on_changes.js &

echo "movieapp-laravel-container is ready!"
/usr/sbin/apache2ctl -D FOREGROUND